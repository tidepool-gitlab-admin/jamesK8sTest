apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    linkerd.io/created-by: linkerd/cli stable-2.5.0
  labels:
    linkerd.io/control-plane-component: web
    linkerd.io/control-plane-ns: linkerd
  name: linkerd-web
  namespace: linkerd
spec:
  replicas: 1
  selector:
    matchLabels:
      linkerd.io/control-plane-component: web
      linkerd.io/control-plane-ns: linkerd
      linkerd.io/proxy-deployment: linkerd-web
  template:
    metadata:
      annotations:
        linkerd.io/created-by: linkerd/cli stable-2.5.0
        linkerd.io/identity-mode: default
        linkerd.io/proxy-version: stable-2.5.0
      labels:
        linkerd.io/control-plane-component: web
        linkerd.io/control-plane-ns: linkerd
        linkerd.io/proxy-deployment: linkerd-web
    spec:
      containers:
      - args:
        - -api-addr=linkerd-controller-api.linkerd.svc.cluster.local:8085
        - -grafana-addr=linkerd-grafana.linkerd.svc.cluster.local:3000
        - -controller-namespace=linkerd
        - -log-level=info
        image: gcr.io/linkerd-io/web:stable-2.5.0
        imagePullPolicy: IfNotPresent
        livenessProbe:
          httpGet:
            path: /ping
            port: 9994
          initialDelaySeconds: 10
        name: web
        ports:
        - containerPort: 8084
          name: http
        - containerPort: 9994
          name: admin-http
        readinessProbe:
          failureThreshold: 7
          httpGet:
            path: /ready
            port: 9994
        resources: null
        securityContext:
          runAsUser: 2103
        volumeMounts:
        - mountPath: /var/run/linkerd/config
          name: config
      - env:
        - name: LINKERD2_PROXY_LOG
          value: warn,linkerd2_proxy=info
        - name: LINKERD2_PROXY_DESTINATION_SVC_ADDR
          value: linkerd-destination.linkerd.svc.cluster.local:8086
        - name: LINKERD2_PROXY_CONTROL_LISTEN_ADDR
          value: 0.0.0.0:4190
        - name: LINKERD2_PROXY_ADMIN_LISTEN_ADDR
          value: 0.0.0.0:4191
        - name: LINKERD2_PROXY_OUTBOUND_LISTEN_ADDR
          value: 127.0.0.1:4140
        - name: LINKERD2_PROXY_INBOUND_LISTEN_ADDR
          value: 0.0.0.0:4143
        - name: LINKERD2_PROXY_DESTINATION_PROFILE_SUFFIXES
          value: svc.cluster.local.
        - name: LINKERD2_PROXY_INBOUND_ACCEPT_KEEPALIVE
          value: 10000ms
        - name: LINKERD2_PROXY_OUTBOUND_CONNECT_KEEPALIVE
          value: 10000ms
        - name: _pod_ns
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: LINKERD2_PROXY_DESTINATION_CONTEXT
          value: ns:$(_pod_ns)
        - name: LINKERD2_PROXY_IDENTITY_DIR
          value: /var/run/linkerd/identity/end-entity
        - name: LINKERD2_PROXY_IDENTITY_TRUST_ANCHORS
          value: '-----BEGIN CERTIFICATE-----

            MIIBgzCCASmgAwIBAgIBATAKBggqhkjOPQQDAjApMScwJQYDVQQDEx5pZGVudGl0

            eS5saW5rZXJkLmNsdXN0ZXIubG9jYWwwHhcNMTkwOTI0MjIyNzE3WhcNMjAwOTIz

            MjIyNzM3WjApMScwJQYDVQQDEx5pZGVudGl0eS5saW5rZXJkLmNsdXN0ZXIubG9j

            YWwwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAAS5E/wA10xCfW26mMoSdT1cq8hp

            oa3XbzHvq6ZPYtok+kWFNoUer4QtqiNL6VPz9ajL4+FmBt7pSBSLOPTcDIAHo0Iw

            QDAOBgNVHQ8BAf8EBAMCAQYwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMC

            MA8GA1UdEwEB/wQFMAMBAf8wCgYIKoZIzj0EAwIDSAAwRQIgB8YdSzN87/3Gkyj0

            fBR78sOiv2XHopmHNY4WxSwClmgCIQD72349dIlEhs/uw7FbQh60U7tu8Payx+A9

            2ksV+77aEg==

            -----END CERTIFICATE-----

            '
        - name: LINKERD2_PROXY_IDENTITY_TOKEN_FILE
          value: /var/run/secrets/kubernetes.io/serviceaccount/token
        - name: LINKERD2_PROXY_IDENTITY_SVC_ADDR
          value: linkerd-identity.linkerd.svc.cluster.local:8080
        - name: _pod_sa
          valueFrom:
            fieldRef:
              fieldPath: spec.serviceAccountName
        - name: _l5d_ns
          value: linkerd
        - name: _l5d_trustdomain
          value: cluster.local
        - name: LINKERD2_PROXY_IDENTITY_LOCAL_NAME
          value: $(_pod_sa).$(_pod_ns).serviceaccount.identity.$(_l5d_ns).$(_l5d_trustdomain)
        - name: LINKERD2_PROXY_IDENTITY_SVC_NAME
          value: linkerd-identity.$(_l5d_ns).serviceaccount.identity.$(_l5d_ns).$(_l5d_trustdomain)
        - name: LINKERD2_PROXY_DESTINATION_SVC_NAME
          value: linkerd-controller.$(_l5d_ns).serviceaccount.identity.$(_l5d_ns).$(_l5d_trustdomain)
        - name: LINKERD2_PROXY_TAP_SVC_NAME
          value: linkerd-tap.$(_l5d_ns).serviceaccount.identity.$(_l5d_ns).$(_l5d_trustdomain)
        image: gcr.io/linkerd-io/proxy:stable-2.5.0
        imagePullPolicy: IfNotPresent
        livenessProbe:
          httpGet:
            path: /metrics
            port: 4191
          initialDelaySeconds: 10
        name: linkerd-proxy
        ports:
        - containerPort: 4143
          name: linkerd-proxy
        - containerPort: 4191
          name: linkerd-admin
        readinessProbe:
          httpGet:
            path: /ready
            port: 4191
          initialDelaySeconds: 2
        resources: null
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
          runAsUser: 2102
        terminationMessagePolicy: FallbackToLogsOnError
        volumeMounts:
        - mountPath: /var/run/linkerd/identity/end-entity
          name: linkerd-identity-end-entity
      initContainers:
      - args:
        - --incoming-proxy-port
        - '4143'
        - --outgoing-proxy-port
        - '4140'
        - --proxy-uid
        - '2102'
        - --inbound-ports-to-ignore
        - 4190,4191
        - --outbound-ports-to-ignore
        - '443'
        image: gcr.io/linkerd-io/proxy-init:v1.1.0
        imagePullPolicy: IfNotPresent
        name: linkerd-init
        resources:
          limits:
            cpu: 100m
            memory: 50Mi
          requests:
            cpu: 10m
            memory: 10Mi
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            add:
            - NET_ADMIN
            - NET_RAW
          privileged: false
          readOnlyRootFilesystem: true
          runAsNonRoot: false
          runAsUser: 0
        terminationMessagePolicy: FallbackToLogsOnError
      serviceAccountName: linkerd-web
      volumes:
      - configMap:
          name: linkerd-config
        name: config
      - emptyDir:
          medium: Memory
        name: linkerd-identity-end-entity
